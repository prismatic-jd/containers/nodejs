# Dockerfile for Node.js with asdf

This repository contains a Dockerfile that defines the steps to create a Docker image with the specified Node.js version installed using the asdf version manager.

## Dockerfile Overview

1. **Base Image**: The Dockerfile uses a base image from the GitLab registry that has the `asdf` version manager pre-installed (version 0.11.2).

2. **Build Argument**: A build-time variable named `CI_COMMIT_REF_NAME` is defined. The value for this variable should be passed during the build process. This is used to determine the Node.js version that will be installed.

3. **Default Shell**: The default shell is set to `/bin/bash` with the `-e` and `-c` options for the subsequent `RUN` command.

4. **Installing Node.js**: The Dockerfile installs the Node.js plugin for the `asdf` version manager using the specified GitHub repository. It then defines a variable named `VERSION_CONDITION` based on the value of `CI_COMMIT_REF_NAME`. If the value is "main", it sets the variable to "latest", otherwise it sets the variable to the value of `CI_COMMIT_REF_NAME`. Node.js is then installed using the `asdf` version manager, with the version determined by the `VERSION_CONDITION` variable.

5. **Setting Node.js Version**: The Dockerfile creates a `.tool-versions` file, which is used by the `asdf` version manager to set the Node.js version for the environment. If `CI_COMMIT_REF_NAME` is "main", it writes the latest Node.js version, otherwise, it writes the value of `CI_COMMIT_REF_NAME`.

## Building the Docker Image
To build the Docker image, use the following command, replacing your_value with the desired value for CI_COMMIT_REF_NAME:

```bash
docker build --build-arg CI_COMMIT_REF_NAME=your_value -t your_image_name .
```

For example, to build the image with the latest Node.js version:
```bash
docker build --build-arg CI_COMMIT_REF_NAME=main -t nodejs-latest .
```

To build the image with a specific Node.js version, for example, 14.17.0:
```bash
docker build --build-arg CI_COMMIT_REF_NAME=14.17.0 -t nodejs-14.17.0 .
```

### Running a Container from the Image
To run a container using the built image, use the following command:
```bash
docker run -it --rm your_image_name /bin/bash
```
Replace your_image_name with the name you used when building the image. This command starts a new container and opens an interactive Bash shell.


## Using the Node.js Environment

Once you have started a container with the interactive Bash shell, you can use the Node.js environment provided by the Docker image. Here are some common operations you might perform:

1. **Check Node.js version**: To check the installed Node.js version, run:
   ```bash
   node -v
   ```
2. Run a Node.js script: To run a Node.js script, first copy the script to the container or mount a volume with the script. Then, use the node command to execute the script:
    ```bash
    node your_script.js
    ```
3. Install and use NPM packages: You can install NPM packages using the npm command:
    ```bash
    npm install package_name
    ```
    After installing the package, you can use it within your Node.js scripts.

4. Exit the container: To exit the container and close the interactive Bash shell, run:
    ```bash
    exit
    ```

## Stopping and Removing the Container
When you finish using the Node.js environment, you may want to stop and remove the container. To do so, follow these steps:

1. List running containers: To list all running containers, run:
  ```bash
  docker ps
  ```
  Find the container ID or name for the container you want to stop.


2. Stop the container: To stop the container, run:
  ```bash
  docker stop container_id_or_name
  ```
  Replace container_id_or_name with the actual container ID or name.

3. Remove the container: To remove the stopped container, run:
  ```bash
  docker rm container_id_or_name
  ```
  Replace container_id_or_name with the actual container ID or name.



## Using hadolint with This Dockerfile

To ensure the Dockerfile follows best practices and guidelines,we use hadolint to lint the Dockerfile in this project. Hadolint is a popular Dockerfile linter that checks for potential issues and violations of Docker best practices.

### Installing hadolint
### Installing hadolint on macOS
1. Install Homebrew if you haven't already:
  ```bash
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  ```
2.  Install hadolint using Homebrew:
#### Installing hadolint on Arch Linux
```bash
brew install hadolint
```

For Arch Linux, you can install hadolint from the [Arch User Repository (AUR)](https://aur.archlinux.org/packages/hadolint-bin/). Use your preferred AUR helper or follow these steps with `yay`:

1. Install `yay` if you haven't already:
  ```bash
   sudo pacman -S --needed git base-devel
   git clone https://aur.archlinux.org/yay-git.git
   cd yay-git
   makepkg -si
  ```
2. Install hadolint using `yay`:
  ```bash
  yay -S hadolint-bin
  ```



### Linting the Dockerfile

After installing hadolint, you can use it to lint the Dockerfile of this project. In the directory containing the Dockerfile, run:

```bash
hadolint Dockerfile
```
Hadolint will analyze the Dockerfile and report any issues it finds. If it detects any problems or violations of best practices, it will display a list of warnings and errors with explanations and possible solutions.

You can then update the Dockerfile to address the identified issues and rerun hadolint to ensure the Dockerfile is in good shape.